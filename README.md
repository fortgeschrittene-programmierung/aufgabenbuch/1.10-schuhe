# 1.06 Schuhe

**Voraussetzung**:
Vorlesung zum Thema generische Klassen, Methoden (Generics).

**Ziel**:
Kennenlernen und Anwenden des Einschränkens von Typvariablen sowie von komplexen, mehrdimensionalen, generischen Datenstrukturen.

**Dauer**:
1,5 Stunden

## Aufgabenstellung
Für die Lagerverwaltung in einem Schuhladen soll ein Programm zur Zuordnung von Schuhen in Schuhschachteln erstellt werden.

### (a) Abstrakte Klasse `Shoes`
Schreiben Sie die abstrakte Klasse `Shoes`. Diese dient als Basisklasse für ein Paar verschiedener Schuharten. Jedes Schuhpaar-Objekt besteht aus:
- einem Namen und
- einer Schuhgröße (es sollen auch halbe Schuhgrößen erlaubt sein).

Erstellen Sie für die Schuharten Stiefel und Sportschuhe eigene Klassen, welche von der Klasse `Shoes` erben. **Hinweis:** Überlegen Sie sich dabei aussagekräftige englische Bezeichnungen für Klassen, Methoden und Variablen.

### (b) Generische Klasse `Box`
Ein beliebiges Objekt soll in einer Schachtel (engl. `Box`) aufbewahrt werden können (nur ein Objekt je Schachtel!). Erstellen Sie dazu eine generische, abstrakte Klasse `Box` mit Methoden zum Hineinlegen und Entfernen eines Objekts und geeigneten Konstruktoren. 

Außerdem soll die Klasse `Box` über eine Methode `public String toString()` verfügen, welche die Schachtel und ihren Inhalt in Zeichenform zurückgibt.

### (c) Klasse `ShoeBox`
Jedes Schuhpaar soll in einer eigenen Schachtel untergebracht werden. Um die generische Klasse `Box` wiederverwenden zu können, soll eine neue Klasse `ShoeBox` erstellt werden, welche von `Box` erbt. 

Um zu verhindern, dass neben Schuhobjekten auch andere Objekte in `ShoeBox` abgelegt werden können, ist es erforderlich, den generischen Platzhalter der Klasse entsprechend einzuschränken.

**Weitere Informationen:**
Erstellen Sie in der `main()`-Methode der Testklasse `App` von den verschiedenen Schuharten jeweils ein Objekt. Außerdem sind `ShoeBox`-Objekte zu erstellen, in denen Sie die Schuhpaare aufbewahren und herausnehmen können. Der Inhalt der `ShoeBox`-Objekte soll auf der Konsole ausgegeben werden.

### (d) Klasse `Shelf`
Erstellen Sie eine Klasse `Shelf`, welche ein Regal für Boxen repräsentiert (diese Klasse ist **nicht** generisch!).

Diese Klasse soll einen Konstruktor besitzen, der es erlaubt, eine feste Höhe und Breite dieses Regals zu definieren. Jedes Regal hat also Höhe x Breite Fächer. In diesem Regal sollen nur Schuhboxen hinzugefügt und entfernt werden können. In jedem Fach hat genau eine Schuhbox Platz. Verwenden Sie `ArrayList<ArrayList<ShoeBox<? extends Shoes>>>` als Typ für die interne Struktur des Regals in der Klasse `Shelf`.

Die Methoden für das Hinzufügen und Entfernen von Schuhboxen sollen neben der eigentlichen Schuhbox auch Parameter enthalten, welche die Position der Schuhbox im Regal bestimmen (Höhe und Breite im Regal).

Schreiben Sie eine `toString()`-Methode für das Regal, welche alle Schuhboxen, inklusive deren Inhalte, als Zeichenkette zurückgibt.

Fangen Sie mögliche Fehler in einer eigens erstellten `BoxException` (erweitert `java.lang.Exception`) ab. Diese soll geworfen werden, wenn beim Hinzufügen einer Schuhbox festgestellt wird, dass das Fach schon belegt ist, bzw. wenn beim Entfernen einer Schuhbox aus einem Fach das Fach leer ist.

**Ergänzung zur `main()`-Methode:**
Ergänzen Sie Ihre `main()`-Methode in der Klasse `App` mit einem Regal, in welchem Sie einige Schuhboxen hinzufügen und auch wieder entfernen. Achten Sie darauf, dass das Regal Schuhboxen von beliebigen Typen lagern kann (also z.B. `ShoeBox<Boots>` oder `ShoeBox<Sneaker>`).